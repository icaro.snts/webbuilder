<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ProjectsWebpagesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ProjectsWebpagesController Test Case
 */
class ProjectsWebpagesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.projects_webpages',
        'app.projects',
        'app.history_projects',
        'app.webpages',
        'app.sessions',
        'app.modules',
        'app.sessions_modules',
        'app.webpages_sessions'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
