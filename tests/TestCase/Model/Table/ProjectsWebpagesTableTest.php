<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProjectsWebpagesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProjectsWebpagesTable Test Case
 */
class ProjectsWebpagesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProjectsWebpagesTable
     */
    public $ProjectsWebpages;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.projects_webpages',
        'app.projects',
        'app.history_projects',
        'app.webpages',
        'app.sessions',
        'app.modules',
        'app.sessions_modules',
        'app.webpages_sessions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProjectsWebpages') ? [] : ['className' => 'App\Model\Table\ProjectsWebpagesTable'];
        $this->ProjectsWebpages = TableRegistry::get('ProjectsWebpages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProjectsWebpages);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
