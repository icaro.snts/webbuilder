<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ThannersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ThannersTable Test Case
 */
class ThannersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ThannersTable
     */
    public $Thanners;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.thanners'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Thanners') ? [] : ['className' => 'App\Model\Table\ThannersTable'];
        $this->Thanners = TableRegistry::get('Thanners', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Thanners);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
