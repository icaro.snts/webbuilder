<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TesteUsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TesteUsersTable Test Case
 */
class TesteUsersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TesteUsersTable
     */
    public $TesteUsers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.teste_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TesteUsers') ? [] : ['className' => 'App\Model\Table\TesteUsersTable'];
        $this->TesteUsers = TableRegistry::get('TesteUsers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TesteUsers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
