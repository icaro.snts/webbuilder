<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WebpagesSessionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WebpagesSessionsTable Test Case
 */
class WebpagesSessionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\WebpagesSessionsTable
     */
    public $WebpagesSessions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.webpages_sessions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('WebpagesSessions') ? [] : ['className' => 'App\Model\Table\WebpagesSessionsTable'];
        $this->WebpagesSessions = TableRegistry::get('WebpagesSessions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WebpagesSessions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
