<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SessionsModulesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SessionsModulesTable Test Case
 */
class SessionsModulesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SessionsModulesTable
     */
    public $SessionsModules;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sessions_modules'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SessionsModules') ? [] : ['className' => 'App\Model\Table\SessionsModulesTable'];
        $this->SessionsModules = TableRegistry::get('SessionsModules', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SessionsModules);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
