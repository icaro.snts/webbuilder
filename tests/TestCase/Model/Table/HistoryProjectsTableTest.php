<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HistoryProjectsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HistoryProjectsTable Test Case
 */
class HistoryProjectsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\HistoryProjectsTable
     */
    public $HistoryProjects;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.history_projects'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('HistoryProjects') ? [] : ['className' => 'App\Model\Table\HistoryProjectsTable'];
        $this->HistoryProjects = TableRegistry::get('HistoryProjects', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->HistoryProjects);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
