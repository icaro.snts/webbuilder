<?php
namespace App\Model\Table;

use App\Model\Entity\Module;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Modules Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Sessions
 */
class ModulesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('modules');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->belongsToMany('Sessions', [
            'foreignKey' => 'module_id',
            'targetForeignKey' => 'session_id',
            'joinTable' => 'sessions_modules'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('title');

        $validator
            ->allowEmpty('description');

        $validator
            ->allowEmpty('directory');

        $validator
            ->allowEmpty('cod_mod');

        $validator
            ->integer('id_framework')
            ->allowEmpty('id_framework');

        $validator
            ->integer('id_feature')
            ->allowEmpty('id_feature');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        return $rules;
    }
}
