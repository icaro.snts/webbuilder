<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * WebpagesSessions Controller
 *
 * @property \App\Model\Table\WebpagesSessionsTable $WebpagesSessions
 */
class WebpagesSessionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $webpagesSessions = $this->paginate($this->WebpagesSessions);

        $this->set(compact('webpagesSessions'));
        $this->set('_serialize', ['webpagesSessions']);
    }

    /**
     * View method
     *
     * @param string|null $id Webpages Session id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $webpagesSession = $this->WebpagesSessions->get($id, [
            'contain' => []
        ]);

        $this->set('webpagesSession', $webpagesSession);
        $this->set('_serialize', ['webpagesSession']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $webpagesSession = $this->WebpagesSessions->newEntity();
        if ($this->request->is('post')) {
            $webpagesSession = $this->WebpagesSessions->patchEntity($webpagesSession, $this->request->data);
            if ($this->WebpagesSessions->save($webpagesSession)) {
                $this->Flash->success(__('The webpages session has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The webpages session could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('webpagesSession'));
        $this->set('_serialize', ['webpagesSession']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Webpages Session id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $webpagesSession = $this->WebpagesSessions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $webpagesSession = $this->WebpagesSessions->patchEntity($webpagesSession, $this->request->data);
            if ($this->WebpagesSessions->save($webpagesSession)) {
                $this->Flash->success(__('The webpages session has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The webpages session could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('webpagesSession'));
        $this->set('_serialize', ['webpagesSession']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Webpages Session id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $webpagesSession = $this->WebpagesSessions->get($id);
        if ($this->WebpagesSessions->delete($webpagesSession)) {
            $this->Flash->success(__('The webpages session has been deleted.'));
        } else {
            $this->Flash->error(__('The webpages session could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
