<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ProjectsWebpages Controller
 *
 * @property \App\Model\Table\ProjectsWebpagesTable $ProjectsWebpages
 */
class ProjectsWebpagesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Projects', 'Webpages']
        ];
        $projectsWebpages = $this->paginate($this->ProjectsWebpages);

        $this->set(compact('projectsWebpages'));
        $this->set('_serialize', ['projectsWebpages']);
    }

    /**
     * View method
     *
     * @param string|null $id Projects Webpage id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $projectsWebpage = $this->ProjectsWebpages->get($id, [
            'contain' => ['Projects', 'Webpages']
        ]);

        $this->set('projectsWebpage', $projectsWebpage);
        $this->set('_serialize', ['projectsWebpage']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $projectsWebpage = $this->ProjectsWebpages->newEntity();
        if ($this->request->is('post')) {
            $projectsWebpage = $this->ProjectsWebpages->patchEntity($projectsWebpage, $this->request->data);
            if ($this->ProjectsWebpages->save($projectsWebpage)) {
                $this->Flash->success(__('The projects webpage has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The projects webpage could not be saved. Please, try again.'));
            }
        }
        $projects = $this->ProjectsWebpages->Projects->find('list', ['limit' => 200]);
        $webpages = $this->ProjectsWebpages->Webpages->find('list', ['limit' => 200]);
        $this->set(compact('projectsWebpage', 'projects', 'webpages'));
        $this->set('_serialize', ['projectsWebpage']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Projects Webpage id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $projectsWebpage = $this->ProjectsWebpages->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $projectsWebpage = $this->ProjectsWebpages->patchEntity($projectsWebpage, $this->request->data);
            if ($this->ProjectsWebpages->save($projectsWebpage)) {
                $this->Flash->success(__('The projects webpage has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The projects webpage could not be saved. Please, try again.'));
            }
        }
        $projects = $this->ProjectsWebpages->Projects->find('list', ['limit' => 200]);
        $webpages = $this->ProjectsWebpages->Webpages->find('list', ['limit' => 200]);
        $this->set(compact('projectsWebpage', 'projects', 'webpages'));
        $this->set('_serialize', ['projectsWebpage']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Projects Webpage id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $projectsWebpage = $this->ProjectsWebpages->get($id);
        if ($this->ProjectsWebpages->delete($projectsWebpage)) {
            $this->Flash->success(__('The projects webpage has been deleted.'));
        } else {
            $this->Flash->error(__('The projects webpage could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
