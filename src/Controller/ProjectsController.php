<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Projects Controller
 *
 * @property \App\Model\Table\ProjectsTable $Projects
 */
class ProjectsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $projects = $this->paginate($this->Projects);

        $this->set(compact('projects'));
        $this->set('_serialize', ['projects']);
    }

    /**
     * View method
     *
     * @param string|null $id Project id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $project = $this->Projects->get($id, [
            'contain' => ['Webpages', 'HistoryProjects']
        ]);
        
        $session = $this->request->session();
        $session->write('project_id', $project->id);
        
        $dir = "../TheProjects";
        if (!is_dir($dir))
            mkdir($dir);
            
        $dir = str_replace(' ', '_', $dir."/".$project->title);
        if (!is_dir($dir))
            mkdir($dir);
        
        $this->set('project', $project);
        $this->set('_serialize', ['project']);

        $this->set('project', $project);
        $this->set('_serialize', ['project']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $project = $this->Projects->newEntity();
        if ($this->request->is('post')) {
            $project = $this->Projects->patchEntity($project, $this->request->data);
            if ($this->Projects->save($project)) {
                $this->Flash->success(__('O projeto foi salvo.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O projeto não pode ser salvo.'));
            }
        }
        
        $webpages = $this->Projects->Webpages->find('list', ['limit' => 200]);
        $this->set(compact('project', 'webpages'));
        $this->set('_serialize', ['project']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Project id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $project = $this->Projects->get($id, [
            'contain' => ['Webpages']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $project = $this->Projects->patchEntity($project, $this->request->data);
            
            if ($this->Projects->save($project)) {
                // if (is_dir($dir))
                // {
                //     $newPageName = $project->title;
                //     echo "old: $oldPageName :: new: $newPageName";
                //     if (file_exists($dir."/".$oldPageName))
                //         rename("$dir/$oldPageName", "$dir/$newPageName");
                // }
                $this->Flash->success(__('O projeto foi salvo.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O projeto não pode ser savo.'));
            }
        }
        $webpages = $this->Projects->Webpages->find('list', ['limit' => 200]);
        $this->set(compact('project', 'webpages'));
        $this->set('_serialize', ['project']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Project id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $project = $this->Projects->get($id);
        if ($this->Projects->delete($project)) {
            $this->Flash->success(__('O projeto foi deletado.'));
        } else {
            $this->Flash->error(__('O projeto não pode ser deletado.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function requestObj($id = null)
    {
        $project = $this->Projects->get($id, [
            'contain' => ['Webpages', 'HistoryProjects']
        ]);

        return $project;
    }
}