<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * HistoryProjects Controller
 *
 * @property \App\Model\Table\HistoryProjectsTable $HistoryProjects
 */
class HistoryProjectsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $historyProjects = $this->paginate($this->HistoryProjects);

        $this->set(compact('historyProjects'));
        $this->set('_serialize', ['historyProjects']);
    }

    /**
     * View method
     *
     * @param string|null $id History Project id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $historyProject = $this->HistoryProjects->get($id, [
            'contain' => []
        ]);

        $this->set('historyProject', $historyProject);
        $this->set('_serialize', ['historyProject']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $historyProject = $this->HistoryProjects->newEntity();
        if ($this->request->is('post')) {
            $historyProject = $this->HistoryProjects->patchEntity($historyProject, $this->request->data);
            if ($this->HistoryProjects->save($historyProject)) {
                $this->Flash->success(__('The history project has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The history project could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('historyProject'));
        $this->set('_serialize', ['historyProject']);
    }

    /**
     * Edit method
     *
     * @param string|null $id History Project id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $historyProject = $this->HistoryProjects->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $historyProject = $this->HistoryProjects->patchEntity($historyProject, $this->request->data);
            if ($this->HistoryProjects->save($historyProject)) {
                $this->Flash->success(__('The history project has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The history project could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('historyProject'));
        $this->set('_serialize', ['historyProject']);
    }

    /**
     * Delete method
     *
     * @param string|null $id History Project id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $historyProject = $this->HistoryProjects->get($id);
        if ($this->HistoryProjects->delete($historyProject)) {
            $this->Flash->success(__('The history project has been deleted.'));
        } else {
            $this->Flash->error(__('The history project could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
