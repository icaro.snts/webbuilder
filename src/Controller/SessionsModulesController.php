<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SessionsModules Controller
 *
 * @property \App\Model\Table\SessionsModulesTable $SessionsModules
 */
class SessionsModulesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $sessionsModules = $this->paginate($this->SessionsModules);

        $this->set(compact('sessionsModules'));
        $this->set('_serialize', ['sessionsModules']);
    }

    /**
     * View method
     *
     * @param string|null $id Sessions Module id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $sessionsModule = $this->SessionsModules->get($id, [
            'contain' => []
        ]);

        $this->set('sessionsModule', $sessionsModule);
        $this->set('_serialize', ['sessionsModule']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $sessionsModule = $this->SessionsModules->newEntity();
        if ($this->request->is('post')) {
            $sessionsModule = $this->SessionsModules->patchEntity($sessionsModule, $this->request->data);
            if ($this->SessionsModules->save($sessionsModule)) {
                $this->Flash->success(__('The sessions module has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sessions module could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('sessionsModule'));
        $this->set('_serialize', ['sessionsModule']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sessions Module id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sessionsModule = $this->SessionsModules->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sessionsModule = $this->SessionsModules->patchEntity($sessionsModule, $this->request->data);
            if ($this->SessionsModules->save($sessionsModule)) {
                $this->Flash->success(__('The sessions module has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sessions module could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('sessionsModule'));
        $this->set('_serialize', ['sessionsModule']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sessions Module id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $sessionsModule = $this->SessionsModules->get($id);
        if ($this->SessionsModules->delete($sessionsModule)) {
            $this->Flash->success(__('The sessions module has been deleted.'));
        } else {
            $this->Flash->error(__('The sessions module could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
