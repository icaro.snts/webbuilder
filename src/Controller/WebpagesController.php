<?php
namespace App\Controller;

use App\Controller\AppController;
require_once "ProjectsController.php";


/**
 * Webpages Controller
 *
 * @property \App\Model\Table\WebpagesTable $Webpages
 */
class WebpagesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Projects']
        ];
        $webpages = $this->paginate($this->Webpages);
        $this->set(compact('webpages'));
        $this->set('_serialize', ['webpages']);
    }

    /**
     * View method
     *
     * @param string|null $id Webpage id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $webpage = $this->Webpages->get($id, [
            'contain' => ['Projects', 'Sessions']
        ]);
        
        // $project_id = $webpage->project_id;
        // foreach ($webpage->projects as $project)
        // {
        //     if($project->id == $project_id)
        //         break;
        // }

        
        $this->set('webpage', $webpage);
        $this->set('_serialize', ['webpage']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $project = new ProjectsController;
        
        $webpage = $this->Webpages->newEntity();
        if ($this->request->is('post')) {
            $webpage = $this->Webpages->patchEntity($webpage, $this->request->data);
            if ($this->Webpages->save($webpage)) {
                // $project2 = $project->requestObj($webpage->$project_id);
                // createArchive($project->title, $webpage->title);
                $this->Flash->success(__('A página foi salva com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('A página não pode ser salva. Tente novamente.'));
            }
        }
        
        $projects = $this->Webpages->Projects->find('list', ['limit' => 200]);
        $sessions = $this->Webpages->Sessions->find('list', ['limit' => 200]);
        $this->set(compact('webpage', 'projects', 'sessions'));
        $this->set('_serialize', ['webpage']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Webpage id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $webpage = $this->Webpages->get($id, [
            'contain' => ['Projects', 'Sessions']
        ]);
        
        $project_id = $webpage->project_id;
        foreach ($webpage->projects as $project)
        {
            if($project->id == $project_id)
                break;
        }
        
        $oldPageName = str_replace(' ', '_', $webpage->title.".PHP");
        $oldPageName = strtolower($oldPageName);
        $dir = "../TheProjects";
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $webpage = $this->Webpages->patchEntity($webpage, $this->request->data);
            if ($this->Webpages->save($webpage)) {
                /*
                if (is_dir($dir))
                {
                    $newPageName = str_replace(' ', '_', $webpage->title.".PHP");
                    $newPageName = strtolower($newPageName);
                    $dir = $dir."/".$project->title;
                    if (file_exists($dir."/".$oldPageName))
                        rename("$dir/$oldPageName", "$dir/$newPageName");
                }
                */
                $this->Flash->success(__('A página foi salva com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('A página não pode ser salva.'));
            }
        }
        $projects = $this->Webpages->Projects->find('list', ['limit' => 200]);
        $sessions = $this->Webpages->Sessions->find('list', ['limit' => 200]);
        $this->set(compact('webpage', 'projects', 'sessions'));
        $this->set('_serialize', ['webpage']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Webpage id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $webpage = $this->Webpages->get($id);
        $project_id = $webpage->project_id;
        
        if($webpage->projects != null)
        {
            foreach ($webpage->projects as $project)
            {
                if($project->id == $project_id)
                    break;
            }
        }
        /*
        $pageName = str_replace(' ', '_', $webpage->title.".PHP");
        $pageName = strtolower($pageName);
        $dir = "../TheProjects";
        */
        if ($this->Webpages->delete($webpage)) {
            $this->Flash->success(__('A página foi deletada.'));
        } else {
            $this->Flash->error(__("A página não pode ser deletada. Tente denovo."));
        }
        
        /*
        if (is_dir($dir) && $project != null)
        {
            $dir = $dir."/".$project->title;
            if (file_exists($dir."/".$pageName))
                unlink($dir."/".$pageName) or die ("Erro ao deletar aquivo.");
        }
        */
        
        return $this->redirect(['action' => 'index']);
    }
    /*
    function createArchive($projectTitle, $webpageTitle)
    {
        $pageName = str_replace(' ', '_', $webpageTitle.".PHP");
        $pageName = strtolower($pageName);
        $dir = "../TheProjects";
        
        if (is_dir($dir))
        {
            $dir = $dir."/".$projectTitle;
            if (!file_exists($dir."/".$pageName))
                $pageFile = fopen($dir."/".$pageName,"w+") or $this->Flash->success(__('Erro ao abrir arquivo.'));
        }
    }
    */
}
