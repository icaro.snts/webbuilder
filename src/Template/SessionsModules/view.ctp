<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sessions Module'), ['action' => 'edit', $sessionsModule->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sessions Module'), ['action' => 'delete', $sessionsModule->id], ['confirm' => __('Tem certeza que deseja deletar # {0}?', $sessionsModule->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sessions Modules'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sessions Module'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="sessionsModules view large-9 medium-8 columns content">
    <h3><?= h($sessionsModule->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($sessionsModule->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Id Session') ?></th>
            <td><?= $this->Number->format($sessionsModule->id_session) ?></td>
        </tr>
        <tr>
            <th><?= __('Id Module') ?></th>
            <td><?= $this->Number->format($sessionsModule->id_module) ?></td>
        </tr>
    </table>
</div>
