<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Sessions Module'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="sessionsModules index large-9 medium-8 columns content">
    <h3><?= __('Sessions Modules') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('id_session') ?></th>
                <th><?= $this->Paginator->sort('id_module') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($sessionsModules as $sessionsModule): ?>
            <tr>
                <td><?= $this->Number->format($sessionsModule->id) ?></td>
                <td><?= $this->Number->format($sessionsModule->id_session) ?></td>
                <td><?= $this->Number->format($sessionsModule->id_module) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $sessionsModule->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $sessionsModule->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $sessionsModule->id], ['confirm' => __('Tem certeza que deseja deletar # {0}?', $sessionsModule->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
