<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $sessionsModule->id],
                ['confirm' => __('Tem certeza que deseja deletar # {0}?', $sessionsModule->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Sessions Modules'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="sessionsModules form large-9 medium-8 columns content">
    <?= $this->Form->create($sessionsModule) ?>
    <fieldset>
        <legend><?= __('Edit Sessions Module') ?></legend>
        <?php
            echo $this->Form->input('id_session');
            echo $this->Form->input('id_module');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
