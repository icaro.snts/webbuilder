<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Projects Webpage'), ['action' => 'edit', $projectsWebpage->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Projects Webpage'), ['action' => 'delete', $projectsWebpage->id], ['confirm' => __('Tem certeza que deseja deletar # {0}?', $projectsWebpage->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Projects Webpages'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Projects Webpage'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Projects'), ['controller' => 'Projects', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Project'), ['controller' => 'Projects', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Webpages'), ['controller' => 'Webpages', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Webpage'), ['controller' => 'Webpages', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="projectsWebpages view large-9 medium-8 columns content">
    <h3><?= h($projectsWebpage->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Project') ?></th>
            <td><?= $projectsWebpage->has('project') ? $this->Html->link($projectsWebpage->project->title, ['controller' => 'Projects', 'action' => 'view', $projectsWebpage->project->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Webpage') ?></th>
            <td><?= $projectsWebpage->has('webpage') ? $this->Html->link($projectsWebpage->webpage->title, ['controller' => 'Webpages', 'action' => 'view', $projectsWebpage->webpage->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($projectsWebpage->id) ?></td>
        </tr>
    </table>
</div>
