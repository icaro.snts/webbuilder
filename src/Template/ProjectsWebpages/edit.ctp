<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $projectsWebpage->id],
                ['confirm' => __('Tem certeza que deseja deletar # {0}?', $projectsWebpage->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Projects Webpages'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Projects'), ['controller' => 'Projects', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Project'), ['controller' => 'Projects', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Webpages'), ['controller' => 'Webpages', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Webpage'), ['controller' => 'Webpages', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="projectsWebpages form large-9 medium-8 columns content">
    <?= $this->Form->create($projectsWebpage) ?>
    <fieldset>
        <legend><?= __('Edit Projects Webpage') ?></legend>
        <?php
            echo $this->Form->input('project_id', ['options' => $projects, 'empty' => true]);
            echo $this->Form->input('webpage_id', ['options' => $webpages, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
