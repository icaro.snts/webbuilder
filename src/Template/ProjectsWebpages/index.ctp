<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Projects Webpage'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Projects'), ['controller' => 'Projects', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Project'), ['controller' => 'Projects', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Webpages'), ['controller' => 'Webpages', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Webpage'), ['controller' => 'Webpages', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="projectsWebpages index large-9 medium-8 columns content">
    <h3><?= __('Projects Webpages') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('project_id') ?></th>
                <th><?= $this->Paginator->sort('webpage_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($projectsWebpages as $projectsWebpage): ?>
            <tr>
                <td><?= $this->Number->format($projectsWebpage->id) ?></td>
                <td><?= $projectsWebpage->has('project') ? $this->Html->link($projectsWebpage->project->title, ['controller' => 'Projects', 'action' => 'view', $projectsWebpage->project->id]) : '' ?></td>
                <td><?= $projectsWebpage->has('webpage') ? $this->Html->link($projectsWebpage->webpage->title, ['controller' => 'Webpages', 'action' => 'view', $projectsWebpage->webpage->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $projectsWebpage->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $projectsWebpage->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $projectsWebpage->id], ['confirm' => __('Tem certeza que deseja deletar # {0}?', $projectsWebpage->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
