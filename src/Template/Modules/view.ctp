<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Novo Módulo'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Editar Módulo'), ['action' => 'edit', $module->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Deletar Módulo'), ['action' => 'delete', $module->id], ['confirm' => __('Tem certeza que deseja deletar # {0}?', $module->id)]) ?> </li>
        <li><?= $this->Html->link(__('Voltar'), ['action' => 'index']) ?> </li>
    </ul>
</nav>
<div class="modules view large-9 medium-8 columns content">
    <h3><?= h($module->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Título') ?></th>
            <td><?= h($module->title) ?></td>
        </tr>
        <tr>
            <th><?= __('Descrição') ?></th>
            <td><?= h($module->description) ?></td>
        </tr>
        <tr>
            <th><?= __('Diretório') ?></th>
            <td><?= h($module->directory) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($module->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Framework') ?></th>
            <td><?= $this->Number->format($module->id_framework) ?></td>
        </tr>
        <tr>
            <th><?= __('Id Feature') ?></th>
            <td><?= $this->Number->format($module->id_feature) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Sessões Relacionadas') ?></h4>
        <?php if (!empty($module->sessions)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Título') ?></th>
                <th><?= __('Descrição') ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
            <?php foreach ($module->sessions as $sessions): ?>
            <tr>
                <td><?= h($sessions->id) ?></td>
                <td><?= h($sessions->title) ?></td>
                <td><?= h($sessions->description) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Visualizar'), ['controller' => 'Sessions', 'action' => 'view', $sessions->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['controller' => 'Sessions', 'action' => 'edit', $sessions->id]) ?>
                    <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Sessions', 'action' => 'delete', $sessions->id], ['confirm' => __('Tem certeza que deseja deletar # {0}?', $sessions->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
