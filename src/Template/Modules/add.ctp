<?php
    session_name('CAKEPHP'); 
    session_start();
?>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Lista de Módulos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Voltar'), ['controller' => 'Frameworks', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="modules form large-9 medium-8 columns content">
    <?= $this->Form->create($module) ?>
    <fieldset>
        <legend><?= __('Adicionar Módulo') ?></legend>
        <?php
            echo $this->Form->input('title');
            echo $this->Form->input('description');
            echo $this->Form->input('directory');
            echo $this->Form->input('cod_mod');
            echo $this->Form->input('id_framework');
            echo $this->Form->input('id_feature');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Criar')) ?>
    <?= $this->Form->end() ?>
</div>
