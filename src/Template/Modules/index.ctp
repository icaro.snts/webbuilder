<?php
    if(!session_start())
    {
        session_name('CAKEPHP'); 
        session_start(); 
    }
    $_FRAMEWORK_ID = $_SESSION['framework_id'];
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Novo Módulo'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Lista de Sessões'), ['controller' => 'Sessions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Voltar'), ['controller' => 'Frameworks', 'action' => 'view', $_FRAMEWORK_ID]) ?></li>
    </ul>
</nav>
<div class="modules index large-9 medium-8 columns content">
    <h3><?= __('Módulos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('Título') ?></th>
                <th><?= $this->Paginator->sort('Descrição') ?></th>
                <th><?= $this->Paginator->sort('Diretório') ?></th>
                <th><?= $this->Paginator->sort('Framework') ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($modules as $module): if ($module->framework_id == $_FRAMEWORK_ID): ?>
            <tr>
                <td><?= $this->Number->format($module->id) ?></td>
                <td><?= h($module->title) ?></td>
                <td><?= h($module->description) ?></td>
                <td><?= h($module->directory) ?></td>
                <td><?= $this->Number->format($module->id_framework) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $module->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $module->id]) ?>
                    <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $module->id], ['confirm' => __('Tem certeza que deseja deletar # {0}?', $module->id)]) ?>
                </td>
            </tr>
            <?php endif; endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Próximo') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
