<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Form->postLink(
                __('Deletar'),
                ['action' => 'delete', $session->id],
                ['confirm' => __('Tem certeza que deseja deletar # {0}?', $session->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Lista de Sessões'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Voltar'), ['action' => 'view', $session->id]) ?></li>
    </ul>
</nav>
<div class="sessions form large-9 medium-8 columns content">
    <?= $this->Form->create($session) ?>
    <fieldset>
        <legend><?= __('Editar Sessão') ?></legend>
        <?php
            echo $this->Form->input('title');
            echo $this->Form->input('description');
            echo $this->Form->input('directory');
            echo $this->Form->input('modules._ids', ['options' => $modules]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('SALVAR')) ?>
    <?= $this->Form->end() ?>
</div>
