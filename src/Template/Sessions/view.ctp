<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Editar'), ['action' => 'edit', $session->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $session->id], ['confirm' => __('Tem certeza que deseja deletar # {0}?', $session->id)]) ?> </li>
        <li><?= $this->Html->link(__('Nova Sessão'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Lista de Modulos'), ['controller' => 'Modules', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Voltar'), ['action' => 'index']) ?> </li>
    </ul>
</nav>
<div class="sessions view large-9 medium-8 columns content">
    <h3><?= h($session->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Título') ?></th>
            <td><?= h($session->title) ?></td>
        </tr>
        <tr>
            <th><?= __('Descrição') ?></th>
            <td><?= h($session->description) ?></td>
        </tr>
        <tr>
            <th><?= __('Diretório') ?></th>
            <td><?= h($session->directory) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($session->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Módulos Relacionados') ?></h4>
        <?php if (!empty($session->modules)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Title') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Directory') ?></th>
                <th><?= __('Cod Mod') ?></th>
                <th><?= __('Id Framework') ?></th>
                <th><?= __('Id Feature') ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
            <?php foreach ($session->modules as $modules): ?>
            <tr>
                <td><?= h($modules->id) ?></td>
                <td><?= h($modules->title) ?></td>
                <td><?= h($modules->description) ?></td>
                <td><?= h($modules->directory) ?></td>
                <td><?= h($modules->cod_mod) ?></td>
                <td><?= h($modules->id_framework) ?></td>
                <td><?= h($modules->id_feature) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Modules', 'action' => 'view', $modules->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Modules', 'action' => 'edit', $modules->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Modules', 'action' => 'delete', $modules->id], ['confirm' => __('Tem certeza que deseja deletar # {0}?', $modules->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
