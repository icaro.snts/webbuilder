<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $webpagesSession->id],
                ['confirm' => __('Tem certeza que deseja deletar # {0}?', $webpagesSession->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Webpages Sessions'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="webpagesSessions form large-9 medium-8 columns content">
    <?= $this->Form->create($webpagesSession) ?>
    <fieldset>
        <legend><?= __('Edit Webpages Session') ?></legend>
        <?php
            echo $this->Form->input('position');
            echo $this->Form->input('id_webpage');
            echo $this->Form->input('id_session');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
