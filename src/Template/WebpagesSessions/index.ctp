<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Webpages Session'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="webpagesSessions index large-9 medium-8 columns content">
    <h3><?= __('Webpages Sessions') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('position') ?></th>
                <th><?= $this->Paginator->sort('id_webpage') ?></th>
                <th><?= $this->Paginator->sort('id_session') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($webpagesSessions as $webpagesSession): ?>
            <tr>
                <td><?= $this->Number->format($webpagesSession->id) ?></td>
                <td><?= $this->Number->format($webpagesSession->position) ?></td>
                <td><?= $this->Number->format($webpagesSession->id_webpage) ?></td>
                <td><?= $this->Number->format($webpagesSession->id_session) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $webpagesSession->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $webpagesSession->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $webpagesSession->id], ['confirm' => __('Tem certeza que deseja deletar # {0}?', $webpagesSession->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
