<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Webpages Session'), ['action' => 'edit', $webpagesSession->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Webpages Session'), ['action' => 'delete', $webpagesSession->id], ['confirm' => __('Tem certeza que deseja deletar # {0}?', $webpagesSession->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Webpages Sessions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Webpages Session'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="webpagesSessions view large-9 medium-8 columns content">
    <h3><?= h($webpagesSession->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($webpagesSession->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Position') ?></th>
            <td><?= $this->Number->format($webpagesSession->position) ?></td>
        </tr>
        <tr>
            <th><?= __('Id Webpage') ?></th>
            <td><?= $this->Number->format($webpagesSession->id_webpage) ?></td>
        </tr>
        <tr>
            <th><?= __('Id Session') ?></th>
            <td><?= $this->Number->format($webpagesSession->id_session) ?></td>
        </tr>
    </table>
</div>
