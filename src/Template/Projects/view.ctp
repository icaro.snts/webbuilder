<?php $_SESSION['project_id'] = $project->id; ?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Editar'), ['action' => 'edit', $project->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $project->id], ['confirm' => __('Tem certeza que deseja deletar # {0}?', $project->id)]) ?> </li>
        <li><?= $this->Html->link(__('Ver Páginas'), ['controller' => 'Webpages', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nova Página'), ['controller' => 'Webpages', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Voltar'), ['controller' => 'Projects', 'action' => 'index']) ?> </li>
    </ul>
</nav>
<div class="projects view large-9 medium-8 columns content">
    <h3><?= h($project->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Título') ?></th>
            <td><?= h($project->title) ?></td>
        </tr>
        <tr>
            <th><?= __('Descrição') ?></th>
            <td><?= h($project->description) ?></td>
        </tr>
    </table>
</div>
