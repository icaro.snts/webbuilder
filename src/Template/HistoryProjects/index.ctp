<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New History Project'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="historyProjects index large-9 medium-8 columns content">
    <h3><?= __('History Projects') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('creation_date') ?></th>
                <th><?= $this->Paginator->sort('id_project') ?></th>
                <th><?= $this->Paginator->sort('id_webpage') ?></th>
                <th><?= $this->Paginator->sort('id_user') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($historyProjects as $historyProject): ?>
            <tr>
                <td><?= $this->Number->format($historyProject->id) ?></td>
                <td><?= h($historyProject->creation_date) ?></td>
                <td><?= $this->Number->format($historyProject->id_project) ?></td>
                <td><?= $this->Number->format($historyProject->id_webpage) ?></td>
                <td><?= $this->Number->format($historyProject->id_user) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $historyProject->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $historyProject->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $historyProject->id], ['confirm' => __('Tem certeza que deseja deletar # {0}?', $historyProject->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
