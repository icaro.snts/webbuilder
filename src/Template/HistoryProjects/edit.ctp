<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $historyProject->id],
                ['confirm' => __('Tem certeza que deseja deletar # {0}?', $historyProject->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List History Projects'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="historyProjects form large-9 medium-8 columns content">
    <?= $this->Form->create($historyProject) ?>
    <fieldset>
        <legend><?= __('Edit History Project') ?></legend>
        <?php
            echo $this->Form->input('creation_date', ['empty' => true]);
            echo $this->Form->input('id_project');
            echo $this->Form->input('id_webpage');
            echo $this->Form->input('id_user');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
