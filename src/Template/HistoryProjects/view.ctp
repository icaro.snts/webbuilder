<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit History Project'), ['action' => 'edit', $historyProject->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete History Project'), ['action' => 'delete', $historyProject->id], ['confirm' => __('Tem certeza que deseja deletar # {0}?', $historyProject->id)]) ?> </li>
        <li><?= $this->Html->link(__('List History Projects'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New History Project'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="historyProjects view large-9 medium-8 columns content">
    <h3><?= h($historyProject->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($historyProject->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Id Project') ?></th>
            <td><?= $this->Number->format($historyProject->id_project) ?></td>
        </tr>
        <tr>
            <th><?= __('Id Webpage') ?></th>
            <td><?= $this->Number->format($historyProject->id_webpage) ?></td>
        </tr>
        <tr>
            <th><?= __('Id User') ?></th>
            <td><?= $this->Number->format($historyProject->id_user) ?></td>
        </tr>
        <tr>
            <th><?= __('Creation Date') ?></th>
            <td><?= h($historyProject->creation_date) ?></td>
        </tr>
    </table>
</div>
