<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Editar'), ['action' => 'edit', $webpage->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $webpage->id], ['confirm' => __('Tem certeza que deseja deletar # {0}?', $webpage->id)]) ?> </li>
        <li><?= $this->Html->link(__('Ver Projetos'), ['controller' => 'Projects', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Voltar'), ['action' => 'index']) ?> </li>
    </ul>
</nav>
<div class="webpages view large-9 medium-8 columns content">
    <h3><?= h($webpage->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Título') ?></th>
            <td><?= h($webpage->title) ?></td>
        </tr>
        <tr>
            <th><?= __('Descrição') ?></th>
            <td><?= h($webpage->description) ?></td>
        </tr>
        <tr>
            <th><?= __('Projeto') ?></th>
            <td><?= $webpage->has('project') ? $this->Html->link($webpage->project->title, ['controller' => 'Projects', 'action' => 'view', $webpage->project->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($webpage->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Home') ?></h4>
        <?= $this->Text->autoParagraph(h($webpage->home)); ?>
    </div>
    <div class="related">
        <h4><?= __('Sessões da Página') ?></h4>
        <?php if (!empty($webpage->sessions)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Título') ?></th>
                <th><?= __('Descrição') ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
            <?php foreach ($webpage->sessions as $sessions): ?>
            <tr>
                <td><?= h($sessions->id) ?></td>
                <td><?= h($sessions->title) ?></td>
                <td><?= h($sessions->description) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Visualizar'), ['controller' => 'Sessions', 'action' => 'view', $sessions->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['controller' => 'Sessions', 'action' => 'edit', $sessions->id]) ?>
                    <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Sessions', 'action' => 'delete', $sessions->id], ['confirm' => __('Tem certeza que deseja deletar # {0}?', $sessions->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
