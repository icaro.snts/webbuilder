<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Voltar'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="webpages form large-9 medium-8 columns content">
    <?= $this->Form->create($webpage) ?>
    <fieldset>
        <legend><?= __('Nova Página') ?></legend>
        <?php
            echo $this->Form->input('title');
            echo $this->Form->input('description');
            echo $this->Form->input('home');
            echo $this->Form->input('project_id', ['options' => $projects, 'empty' => false]);
            echo $this->Form->input('sessions._ids', ['options' => $sessions]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Criar')) ?>
    <?= $this->Form->end() ?>
</div>
