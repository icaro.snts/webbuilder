<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Webpage'), ['action' => 'edit', $webpage->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Webpage'), ['action' => 'delete', $webpage->id], ['confirm' => __('Tem certeza que deseja deletar # {0}?', $webpage->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Webpages'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Webpage'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Projects'), ['controller' => 'Projects', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Project'), ['controller' => 'Projects', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List History Projects'), ['controller' => 'HistoryProjects', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New History Project'), ['controller' => 'HistoryProjects', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sessions'), ['controller' => 'Sessions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Session'), ['controller' => 'Sessions', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="webpages view large-9 medium-8 columns content">
    <h3><?= h($webpage->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Title') ?></th>
            <td><?= h($webpage->title) ?></td>
        </tr>
        <tr>
            <th><?= __('Description') ?></th>
            <td><?= h($webpage->description) ?></td>
        </tr>
        <tr>
            <th><?= __('Project') ?></th>
            <td><?= $webpage->has('project') ? $this->Html->link($webpage->project->title, ['controller' => 'Projects', 'action' => 'view', $webpage->project->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($webpage->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Home') ?></h4>
        <?= $this->Text->autoParagraph(h($webpage->home)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Sessions') ?></h4>
        <?php if (!empty($webpage->sessions)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Title') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Directory') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($webpage->sessions as $sessions): ?>
            <tr>
                <td><?= h($sessions->id) ?></td>
                <td><?= h($sessions->title) ?></td>
                <td><?= h($sessions->description) ?></td>
                <td><?= h($sessions->directory) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Sessions', 'action' => 'view', $sessions->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Sessions', 'action' => 'edit', $sessions->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Sessions', 'action' => 'delete', $sessions->id], ['confirm' => __('Tem certeza que deseja deletar # {0}?', $sessions->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
