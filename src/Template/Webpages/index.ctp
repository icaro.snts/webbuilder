<?php
    session_name('CAKEPHP'); 
    session_start(); 
    $_PROJECT_ID = $_SESSION['project_id'];
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Nova Página'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Todos Projetos'), ['controller' => 'Projects', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Voltar'), ['controller' => 'Projects', 'action' => 'view', $_PROJECT_ID]) ?></li>
    </ul>
</nav>
<div class="webpages index large-9 medium-8 columns content">
    <h3><?= __('Páginas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('Título') ?></th>
                <th><?= $this->Paginator->sort('Descrição') ?></th>
                <th><?= $this->Paginator->sort('Projeto') ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($webpages as $webpage): ?>
            <?php if($webpage->project_id == $_PROJECT_ID): ?>
            <tr>
                <td><?= $this->Number->format($webpage->id) ?></td>
                <td><?= h($webpage->title) ?></td>
                <td><?= h($webpage->description) ?></td>
                <td><?= $webpage->has('project') ? $this->Html->link($webpage->project->title, ['controller' => 'Projects', 'action' => 'view', $webpage->project->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $webpage->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $webpage->id]) ?>
                    <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $webpage->id], ['confirm' => __('Tem certeza que deseja deletar # {0}?', $webpage->id)]) ?>
                </td>
            </tr>
            <?php endif; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('próximo') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
