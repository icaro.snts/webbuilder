<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Voltar'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="frameworks form large-9 medium-8 columns content">
    <?= $this->Form->create($framework) ?>
    <fieldset>
        <legend><?= __('Adicionar Framework') ?></legend>
        <?php
            echo $this->Form->input('title');
            echo $this->Form->input('version');
            echo $this->Form->input('url_doc');
            echo $this->Form->input('link_css');
            echo $this->Form->input('link_js');
            echo $this->Form->input('cod_fw');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Criar')) ?>
    <?= $this->Form->end() ?>
</div>
