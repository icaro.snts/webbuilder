<?php 
    if(!session_start())
    {
        session_name('CAKEPHP'); 
        session_start(); 
    }
    $_SESSION['framework_id'] = $framework->id;
    $_SESSION['framework_title'] = $framework->title;
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Criar Módulo'), ['controller' => 'Modules', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Lista de Módulos'), ['controller' => 'Modules', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Editar Framework'), ['action' => 'edit', $framework->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Deletar Framework'), ['action' => 'delete', $framework->id], ['confirm' => __('Tem certeza que deseja deletar # {0}?', $framework->id)]) ?> </li>
        <li><?= $this->Html->link(__('Voltar'), ['action' => 'index']) ?> </li>
    </ul>
</nav>
<div class="frameworks view large-9 medium-8 columns content">
    <h3><?= h($framework->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Título') ?></th>
            <td><?= h($framework->title) ?></td>
        </tr>
        <tr>
            <th><?= __('Versão') ?></th>
            <td><?= h($framework->version) ?></td>
        </tr>
        <tr>
            <th><?= __('URL DOC') ?></th>
            <td><?= h($framework->url_doc) ?></td>
        </tr>
        <tr>
            <th><?= __('URL-CDN CSS') ?></th>
            <td><?= h($framework->link_css) ?></td>
        </tr>
        <tr>
            <th><?= __('URL-CDN JS') ?></th>
            <td><?= h($framework->link_js) ?></td>
        </tr>
        <tr>
            <th><?= __('Código') ?></th>
            <td><?= h($framework->cod_fw) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($framework->id) ?></td>
        </tr>
    </table>
</div>
<?php 
    $_SESSION['framework_id'] = $framework->id; 
    $_SESSION['framework_title'] = $framework->title;

?>
