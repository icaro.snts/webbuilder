<?php
    if(!session_start())
    {
        session_name('CAKEPHP'); 
        session_start();
    }
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Criar Framework'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="frameworks index large-9 medium-8 columns content">
    <h3><?= __('Frameworks') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('Títlo') ?></th>
                <th><?= $this->Paginator->sort('Versão') ?></th>
                <th><?= $this->Paginator->sort('URL DOC') ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($frameworks as $framework): ?>
            <tr>
                <td><?= $this->Number->format($framework->id) ?></td>
                <td><?= h($framework->title) ?></td>
                <td><?= h($framework->version) ?></td>
                <td><?= h($framework->url_doc) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $framework->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $framework->id]) ?>
                    <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $framework->id], ['confirm' => __('Tem certeza que deseja deletar # {0}?', $framework->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Próximo') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
